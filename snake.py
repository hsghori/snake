import curses
import os
import time

def set_up_console():
	global main_screen
	main_screen = curses.initscr()
	curses.start_color()
	curses.init_pair(1, curses.COLOR_RED, curses.COLOR_WHITE)
	main_screen.leaveok(True) #hides cursor
	curses.noecho() #prevents user input from being displayed to console
	curses.cbreak() #program responds at each keystroke, doesn't wait for enter
	main_screen.keypad(1) #allows user to use special keys

def open_leaderboard():
	global board_rA
	board_rA = []
	with open('leaderboard.txt', 'r') as board:
		for line in board:
			line_rA = line.split(' ')
			board_rA.append(line_rA[0], line_rA[1])
		board.close()

def leaderboard():
	tl = 'Leaderboard'
	open_leaderboard()
	main_screen.addstr(0, screen_dims[1] / 2 - len(tl) / 2, tl)
	for i in range (0, len(board_rA)):
		main_screen.addstr(i + 1, 0, str(i + 1) + ': ' + str(board_rA[0]) + ' ' + str(board_rA[1]))
	return

def add_to_leaderboard(time, name):
	count = 1
	open_leaderboard()
	for b in board_rA:
		if time < int(b[0]):
			board_rA.insert((time, name))
			break
		count += 1
		if count >= 10:
			break
	board = open('leaderboard.txt', 'w')
	for b in board_rA:
		board.write(str(b[0]) + ' ' + str(b[1]))
	board.write()

def menu():
	main_screen.nodelay(0) #user input is blocking
	main_screen.addstr(screen_dims[0] / 2 - 1, screen_dims[1] / 2 - len('1. Play') / 2, '1. Play')
	main_screen.addstr(screen_dims[0] / 2, screen_dims[1] / 2 - len('2. Leaderboard') / 2, '2. Leaderboard')
	main_screen.addstr(screen_dims[0] / 2 + 1, screen_dims[1] / 2 - len('3. Quit') / 2, '3. Quit')
	main_screen.refresh()
	while True:
		key = main_screen.getch()
		if key == ord('1'):
			return
		elif key == ord('2'):
			leaderboard()
		elif key == ord('3'):
			end()

def game():
	main_screen.nodelay(1) #user input is non-blocking
	try:
		#set up playing screen
		snake_coords = []
		current_dir = 'right'
		delay = 0.2
		count = 0
		for i in range (-3, 3):
			snake_coords.append((screen_dims[0] / 2, screen_dims[1] / 2 + i))
			main_screen.addstr(screen_dims[0] / 2, screen_dims[1] / 2 + i, 'a', curses.A_BOLD | curses.color_pair(1))
		for i in range (0, screen_dims[1]):
			main_screen.addstr(2, i, '-')
		main_screen.addstr(0, 0, 'Time: 0:00', curses.A_BOLD)
		main_screen.refresh()
		while True:
			key = main_screen.getch()
			head = snake_coords[0]
			if key == curses.KEY_UP and current_dir != 'down':
				new_head = (head[0] - 1, head[1])
				current_dir = 'up'
			elif key == curses.KEY_DOWN and current_dir != 'up':
				new_head = (head[0] + 1, head[1])
				current_dir = 'down'
			elif key == curses.KEY_LEFT and current_dir != 'right':
				new_head = (head[0], head[1] - 1)
				current_dir = 'left'
			elif key == curses.KEY_RIGHT and current_dir != 'left':
				new_head = (head[0], head[1] + 1)
				current_dir = 'right'
			else:
				if current_dir == 'up':
					new_head = (head[0] - 1, head[1])
				elif current_dir == 'down':
					new_head = (head[0] + 1, head[1])
				elif current_dir == 'left':
					new_head = (head[0], head[1] - 1)
				elif current_dir == 'right':
					new_head = (head[0], head[1] + 1)
			if new_head[1] < 0 or new_head[1] >= screen_dims[1] or main_screen.inch(new_head[0], new_head[1]) == ord('a') or main_screen.inch(new_head[0], new_head[1]) == ord('-'):
				add_to_leaderboard(count, 'temp')
				menu()
			main_screen.clear()
			if count % 10 != 0 or count < 1: #adds a new piece every ten seconds
				snake_coords.pop()
			snake_coords.insert(0, new_head)
			#redraw screen
			for i in range (0, screen_dims[1]):
				main_screen.addstr(2, i, '-')
			for coord in snake_coords:
				main_screen.addstr(coord[0], coord[1], 'a', curses.A_BOLD | curses.color_pair(1))
			main_screen.addstr(0, 0, 'Time: %s:%02i' % (int(count / 60), int(count % 60)), curses.A_BOLD)
			main_screen.refresh()
			time.sleep(delay)
			count += delay
			if count % 10 == 0 and int(count) != 0 and delay > 0:
				delay == delay - 0.05
	except KeyboardInterrupt: #exits safely on force quit
		end()

def end():
	curses.nocbreak()
	main_screen.keypad(0)
	curses.echo()
	curses.endwin()
	os.system('stty sane')


def main():
	global screen_dims
	set_up_console()
	screen_dims = main_screen.getmaxyx()
	menu()
	game()
	end()
	
if __name__ == '__main__':
	main()